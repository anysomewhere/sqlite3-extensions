#include <sqlite3ext.h>
SQLITE_EXTENSION_INIT1
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "base58.h"

static void base58_to_binary_func(sqlite3_context *context, int argc, sqlite3_value **argv) {
	size_t b58_size = sqlite3_value_bytes(argv[0]);
	const char *b58 = sqlite3_value_text(argv[0]);
	size_t data_size = b58_size;
	void *data = sqlite3_malloc64(data_size);
	
	if (data == NULL) {
		sqlite3_result_error_nomem(context);
	}
	
	if (!b58tobin(data, &data_size, b58, b58_size)) {
		sqlite3_result_error(context, "base58 decoding failed", -1);
		sqlite3_free(data);
		return;
	}
	
	sqlite3_result_blob(context, data, data_size, SQLITE_TRANSIENT);
	sqlite3_free(data);
}

static void base58_check_func(sqlite3_context *context, int argc, sqlite3_value **argv) {
	size_t b58_size = sqlite3_value_bytes(argv[0]);
	const char *b58 = sqlite3_value_text(argv[0]);
	size_t data_size = sqlite3_value_bytes(argv[1]);
	const void *data = sqlite3_value_blob(argv[1]);
	
	if (data == NULL) {
		sqlite3_result_error_nomem(context);
	}
	
	int version;
	if ((version = b58check(data, data_size, b58, b58_size)) < 0) {
		sqlite3_result_error(context, "base58 check failed", -1);
		return;
	}
	
	sqlite3_result_int(context, version);
}

static void base58_encode_func(sqlite3_context *context, int argc, sqlite3_value **argv) {
	size_t data_size = sqlite3_value_bytes(argv[0]);
	const void *data = sqlite3_value_blob(argv[0]);
	size_t b58_size = data_size * 2 + 1;
	char *b58 = (char *)sqlite3_malloc64(b58_size);
	
	if (b58 == NULL) {
		sqlite3_result_error_nomem(context);
	}

	if (!b58enc(b58, &b58_size, data, data_size)) {
		sqlite3_result_error(context, "base58 encoding failed", -1);
		sqlite3_free(b58);
		return;
	}
	
	sqlite3_result_text(context, b58, -1, SQLITE_TRANSIENT);
	sqlite3_free(b58);
}

static void base58_check_encode_func(sqlite3_context *context, int argc, sqlite3_value **argv) {
	int version = sqlite3_value_int(argv[0]);
	size_t data_size = sqlite3_value_bytes(argv[1]);
	const void *data = sqlite3_value_blob(argv[1]);
	size_t b58_size = data_size * 2 + 1;
	char *b58 = (char *)sqlite3_malloc64(b58_size);
	
	if (b58 == NULL) {
		sqlite3_result_error_nomem(context);
	}

	if (!b58check_enc(b58, &b58_size, version, data, data_size)) {
		sqlite3_result_error(context, "base58 check encoding failed", -1);
		sqlite3_free(b58);
		return;
	}
	
	sqlite3_result_text(context, b58, -1, SQLITE_TRANSIENT);
	sqlite3_free(b58);
}

#ifdef _WIN32
__declspec(dllexport)
#endif
int sqlite3_base_init(sqlite3 *db, char **pzErrMsg, const sqlite3_api_routines *pApi) {
	int rc = SQLITE_OK;
	SQLITE_EXTENSION_INIT2(pApi);
	(void)pzErrMsg;  /* Unused parameter */
	rc = sqlite3_create_function(db, "base58_to_binary", 1, SQLITE_UTF8|SQLITE_DETERMINISTIC, 0, base58_to_binary_func, 0, 0);
	if (rc == SQLITE_OK) {
		rc = sqlite3_create_function(db, "base58_check", 2, SQLITE_UTF8|SQLITE_DETERMINISTIC, 0, base58_check_func, 0, 0);
	}
	if (rc == SQLITE_OK) {
		rc = sqlite3_create_function(db, "base58_encode", 1, SQLITE_UTF8|SQLITE_DETERMINISTIC, 0, base58_encode_func, 0, 0);
	}
	if (rc == SQLITE_OK) {
		rc = sqlite3_create_function(db, "base58_check_encode", 2, SQLITE_UTF8|SQLITE_DETERMINISTIC, 0, base58_check_encode_func, 0, 0);
	}
	return SQLITE_OK;
}